// Prevent default drag

(function() {

	const targetPrevent = $('img, a, .button, .slide_bg-text');

	targetPrevent.on('dragstart', function(e) { e.preventDefault(); });

}());