// Tariffs action

(function() {

	$('.tariffs_choice_item').hover(function (e) {
		const tariffIndex = e.target.dataset.tariff;

		const itemInfo = $('.tariffs_info_item');
		const allInfo = itemInfo;
		const targetInfo = allInfo[tariffIndex];

		$(allInfo).removeClass('is-active');
		$(targetInfo).addClass('is-active');
	});

}());