// Sound button

(function () {
	const soundButton = $('#soundButton');
	const sound = $('#sound');

	soundButton.click(function(e) {
		soundButton.toggleClass('is-muted');

		if (soundButton.hasClass('is-muted')) {
			sound[0].pause();
		} else {
			sound[0].play();
		}
	});

}());