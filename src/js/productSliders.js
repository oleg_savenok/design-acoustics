// Products slider scripts

(function () {

	// Slick slider init

	const commonOptions = {
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		draggable: false,
		//accessibility: false,
		speed: 1000,
		easing: (0.77, 0, 0.175, 1),
	};

	$('#productsImagesSlider').slick({
		...commonOptions,
		asNavFor: '#productsNavSlider',
		fade: true,
	});

	$('#productsNavSlider').slick({
		...commonOptions,
		asNavFor: '#productsImagesSlider',
	});

	// Category nav

	$('.products_category_item').click(function (e) {
		e.preventDefault();

		// Change slide
		const slideIndex = $(this).data('slide');
		$('#productsImagesSlider').slick('slickGoTo', slideIndex);

		// Add active line
		if ($('#productsImagesSlider').slick('slickCurrentSlide') == slideIndex) {
			$('.products_category_item').removeClass('is-active');
			$(this).addClass('is-active');
		};
	});

	// Product item hover

	$('.products_product_nav_item li').hover(function (e) {
		const productsIndex = e.target.parentElement.parentElement.dataset.products;
		const productIndex = e.target.dataset.product;

		const itemImg = $('.products_product_images_item');
		const allImg = itemImg[productsIndex].children;
		const targetImg = allImg[productIndex];

		$(allImg).removeClass('is-active');
		$(targetImg).addClass('is-active');
	});

}());
