// Common file

// Products sliders
import "./productSliders";

// fullPage.js initialization options and events
import "./fullPage";

// Nav open/close action
import "./navAction";

// Sound on site action
import "./soundAction";

// Prevent default drag
import "./preventDrag";

// About open more text
import "./aboutOpenText";

// About video action
import "./videoAction";

// Tariff info action
import "./tariffsAction";